variable shape{
    default = "VM.Standard.E2.1"
}
variable compartment_id{}
variable subnet{}
variable public_key_path{}
variable image{}
variable install_script{}

resource "oci_core_instance" "oracle_linux" {
    # Required
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[2].name
    compartment_id = var.compartment_id
    shape = var.shape
    source_details {
        source_id = var.image
        source_type = "image"
    }

    # Optional
    display_name = "First Oracle Linux box"
    create_vnic_details {
        assign_public_ip = true
        subnet_id = var.subnet
    }
    metadata = {
        ssh_authorized_keys = file(var.public_key_path)
    } 
    preserve_boot_volume = false
}

resource "null_resource" "remote-exec" {
  depends_on = [oci_core_instance.oracle_linux]

  provisioner "remote-exec" {
    connection {
      agent       = false
      timeout     = "30m"
      host        = oci_core_instance.oracle_linux.public_ip
      user        = "opc"
      private_key = "${file(var.ssh_private_key)}"
    }
    script = var.install_script
  }
}


output "public_ip" {
  value = oci_core_instance.oracle_linux.public_ip
}